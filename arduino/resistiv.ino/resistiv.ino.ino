/* проверка работы датчика силы.

  Подключите один контакт датчика силы к 5 В, второй - к аналоговому пина Arduino Analog 0.

  Потом подключите один конец резистора 10 КОм между аналоговым пином 0 и землей. Подключите светодиод через резистор к земле.

  Для более детальной информации смотрите статью на сайте: www.ladyada.net/learn/sensors/fsr.html */

int fsrAnalogPin = 0; // датчик силы подключен к пину analog 0

int LEDpin = 13; // подключаем красный светодиод к контакту 11 (ШИМ выход)

int fsrReading; // аналоговые значения с датчика силы

int LEDbrightness;

int i;

String  output_str;

void setup(void) {

  Serial.begin(9600); // будем отправлять информацию в серийный монитор в Arduino IDE

  pinMode(LEDpin, OUTPUT);

}

void loop(void) {
//
//  fsrReading = analogRead(fsrAnalogPin);
//  Serial.print(1);
//  Serial.print(",");
//  Serial.println(fsrReading);
  output_str = "";
  if (i > 255)
  {
    i = 0;
  }
  else
  {
    i ++;
  }

  for (int j = 0; j <= 4; j++) {
    fsrReading = analogRead(j);
    output_str = output_str + String(fsrReading) + ",";
//    Serial.print(i);
//    Serial.print(",");
  }
   Serial.println(output_str);
  

  //Serial.print("Analog reading = ");

  //Serial.println(fsrReading);

  // надо масштабировать диапазон аналоговых значений (0-1023) к диапазону,

  // который используется функцией analogWrite (0-255) с помощью команды map!

  LEDbrightness = map(fsrReading, 0, 1023, 0, 255);

  // светодиод горят ярче, если вы прилагаете большую нагрузку
  analogWrite(LEDpin, LEDbrightness);

  delay(200);

}
